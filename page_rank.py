import json
import time

D_FACTOR = 0.15

with open('json_data_inner.json', encoding='utf-8') as f1, open('json_data_outer.json', encoding='utf-8') as f2:
    inner_dict: dict = json.load(f1)
    outer_dict: dict = json.load(f2)
    indices_list = inner_dict.keys()
    list_len = len(indices_list)
    page_rank: dict = dict.fromkeys(inner_dict, 1/list_len)



for i in range(50):
    for index in page_rank:
        try:
            inner_sum = 0
            for j in inner_dict[index]:
                inner_sum += page_rank[j]/len(outer_dict[j])
            page_rank[index] = D_FACTOR / list_len + (1 - D_FACTOR) * inner_sum
        except:
            continue

norm_page_rank = page_rank.copy()

page_rank_sum = 0
for index in page_rank:
    page_rank_sum += page_rank[index]

for index in page_rank:
    page_rank[index] = format(page_rank[index], '.8f')
    norm_page_rank[index] = format(norm_page_rank[index]/page_rank_sum, '.8f')


with open('result.json', 'w', encoding='utf-8') as file:
    json.dump(page_rank, file, indent=4)

with open('norm_result.json', 'w', encoding='utf-8') as file:
    json.dump(norm_page_rank, file, indent=4)

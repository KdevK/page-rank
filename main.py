import json

from bs4 import BeautifulSoup
import requests
import re
from tqdm import tqdm
from json import dump

headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'uk,en-US;q=0.9,en;q=0.8,ru;q=0.7',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'
}

url = 'http://fatsecret.com'

inner_dict = {}
outer_dict = {}


def find_links(url):
    # print(url)
    # global inner_dict
    # global outer_number_dict
    response = requests.get(url.strip(), timeout=5, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')

    links = []

    for link in soup.find_all('a', href=True):
        try:
            to_add = re.sub('#.*', '',  link['href'])
            # print(f'to add is {to_add}')
            if link['href'][0:10] == 'javascript':
                # print("link caught!" + re.match('https?://(\w*\.)*\w+', url.strip()).group(0))
                continue
            elif link['href'][0] != 'h':
                if link['href'][0] != '/':
                    to_add = '/' + to_add
                to_add = re.match('https?://(\w*\.)*\w+', url.strip()).group(0) + to_add
            # to_add = requests.get(to_add, timeout=2).url
            # print(f'to add is {to_add} and url strip is {url.strip()}')

            orig_url = url.strip()

            to_add = re.sub('(\?|\#|=|&).*\w', '', to_add)
            to_add = re.sub('www\.', '', to_add)
            to_add = re.sub('https', 'http', to_add)
            to_add = re.sub('/$', '', to_add)

            orig_url = re.sub('(\?|\#|=|&).*\w', '', orig_url)
            orig_url = re.sub('www\.', '', orig_url)
            orig_url = re.sub('https', 'http', orig_url)
            orig_url = re.sub('/$', '', orig_url)

            if to_add == orig_url:
                continue

            links.append(to_add)
            # print(to_add + " was appended")


            if to_add not in inner_dict:
                inner_dict[to_add] = []
            if orig_url not in inner_dict[to_add]:
                inner_dict[to_add].append(orig_url)

            if orig_url not in outer_dict:
                outer_dict[orig_url] = []
            if to_add not in outer_dict[orig_url]:
                outer_dict[orig_url].append(to_add)


            # print()
        except:
            continue

    new_links = list(set(links))
    return new_links


# with open('first_iter.txt', 'w', encoding='utf-8') as file:
#     for line in find_links(url):
#         file.write(line)
#         file.write('\n')
#
#
# with open('first_iter.txt', 'r', encoding='utf-8') as read_file, open('second_iter.txt', 'w', encoding='utf-8') as write_file:
#     for line in tqdm(read_file):
#         try:
#             for link in find_links(line):
#                 write_file.write(link)
#                 write_file.write('\n')
#         except Exception as e:
#             print(e)
#             continue
#
# with open('second_iter.txt', 'r', encoding='utf-8') as read_file, open('third_iter.txt', 'w', encoding='utf-8') as write_file:
#     for line in tqdm(read_file):
#         try:
#             for link in find_links(line):
#                 write_file.write(link)
#                 write_file.write('\n')
#         except Exception as e:
#             print(e)
#             continue



# with open('json_data_inner.json', 'w') as outfile:
#     dump(inner_dict, outfile, indent=4)

# with open('json_data_outer.json', 'w') as outfile:
#     dump(outer_dict, outfile, indent=4)


with open('json_data_inner.json', encoding='utf-8') as f1, open('indices.txt', 'w', encoding='utf-8') as f2:
    temp_dict: dict = json.load(f1)
    for key in temp_dict:
        f2.write(f'{key}\n')
